<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE Rule PUBLIC "sailpoint.dtd" "sailpoint.dtd">
<Rule language="beanshell" name="BNT-SuccessFactorsEmployees-ManagerCorrelation" type="ManagerCorrelation">
  <Description>A rule called to help IdentityIQ take a value from the mapped manager field and map it to an existing IdentityIQ identity.

Just like identity correlation, the rule needs to return a map that contains some values that will guide the correlator on how to try to find the identity.  It can also do the lookup manually and just returned the identity.</Description>
  <Signature returnType="Map">
    <Inputs>
      <Argument name="log" type="org.apache.commons.logging.Log">
        <Description>
          The log object associated with the SailPointContext.
        </Description>
      </Argument>
      <Argument name="context" type="sailpoint.api.SailPointContext">
        <Description>
          A sailpoint.api.SailPointContext object that can be used to query the database if necessary.
        </Description>
      </Argument>
      <Argument name="environment" type="Map">
        <Description>
          Arguments passed to the aggregation task.
        </Description>
      </Argument>
      <Argument name="application">
        <Description>
          The application the aggregated account is from.
        </Description>
      </Argument>
      <Argument name="instance">
        <Description>
          Optional instance identifier within the application.
        </Description>
      </Argument>
      <Argument name="connector">
        <Description>
          A connector to the attribute source.
        </Description>
      </Argument>
      <Argument name="link">
        <Description>
          Account link of the referencing identity.
        </Description>
      </Argument>
      <Argument name="managerAttributeValue">
        <Description>
          Attribute value that is stored in the manager attribute.
        </Description>
      </Argument>
    </Inputs>
    <Returns>
      <Argument name="identityName">
        <Description>
          The name of an Identity object.
        </Description>
      </Argument>
      <Argument name="identity">
        <Description>
          A fully resolved Identity object if the rule wants
          to do its own queries to locate the identity.
        </Description>
      </Argument>
      <Argument name="identityAttributeName">
        <Description>
          The name of the extended attribute that can be used
          to locate an existing identity.
        </Description>
      </Argument>
      <Argument name="identityAttributeValue">
        <Description>
          The value of the named extended attribute that can be used
          to locate an existing identity. This attribute is used
          together with the identityAttributeName argument.
        </Description>
      </Argument>
    </Returns>
  </Signature>
  <Source><![CDATA[
  import sailpoint.object.Identity;
  import sailpoint.object.QueryOptions;
  import sailpoint.object.Filter;
  import org.apache.log4j.Logger;
  
  
  Logger logger = Logger.getLogger("biontech.rule.BNT-SuccessFactorsEmployees-ManagerCorrelation");
  logger.debug("--- ENTRY ---");
  
  Map resultMap = null;
  String bntID = link.getAttribute("Person ID External");
  String localEmploymentType = link.getAttribute("LocalEmploymentType");
  String managerBNTID = link.getAttribute("BNTManagerPersonIDExternal");
  String managerPersonID = link.getAttribute("ManagerID"); //OOTB connector is pointing to PersonID
  List filters = new ArrayList();
  
  logger.debug("manager BNT ID for " + bntID + " is " + managerBNTID);
  
  // default is BNTManagerPersonIDExternal, but if null then try other attrs
  if (managerBNTID == null) {
    managerBNTID = link.getAttribute("BNTManagerID");
    if (managerBNTID != null && managerBNTID.equalsIgnoreCase("NO_MANAGER")) managerBNTID = null;
    logger.debug("manager BNT ID for " + bntID + " is " + managerBNTID);
    if (managerBNTID == null && managerPersonID == null) {
      if (localEmploymentType != null && localEmploymentType.equals("6")) {
        // for externals
        managerBNTID = link.getAttribute("WorkOrderOwnerID");
        logger.debug("manager BNT ID from WorkOrder for " + bntID + " is " + managerBNTID);
      }
    }
  }
  
  // set up the attribute to search for IIQ manager identity
  if (managerBNTID != null) {
    filters.add(Filter.eq("bntId", managerBNTID));
  } else if (managerPersonID != null) {
    filters.add(Filter.eq("personId", managerPersonID));
  }
  
  if (filters.size() > 0) {
    // search identity to correlate
    filters.add(Filter.eq("workgroup", false));
    QueryOptions queryOptions = new QueryOptions();
    queryOptions.addFilter(Filter.and(filters));
    
    List identities = context.getObjects(Identity.class, queryOptions);
    
    if (identities.size() == 1) {
      Identity managerIdentity = identities.get(0);
      resultMap = new HashMap();
      resultMap.put("identity", managerIdentity);
      logger.debug("found manager " + managerIdentity.getName());
    } else if (identities.size() == 0) {
      logger.warn("Manager identity not found in IIQ for " + bntID);
    } else {
      logger.warn("Multiple manager identities found in IIQ for " + bntID);
    }
  } else {
    logger.warn("No manager mapped in SuccessFactors for user with BNT ID = " + bntID);
  }
  
  return resultMap;
  ]]></Source>
</Rule>